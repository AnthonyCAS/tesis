# -*- coding: utf-8 -*-
"""
Created on Mon Jun 06 10:29:18 2016

@author: Anthony Ccapira Avendaño
"""

import pystocks
import datetime
import sys
import pandas as pd
import matplotlib.pyplot as plt
import locale
locale.setlocale(locale.LC_ALL,'en_US.UTF-8')
from pylab import *

fout = 'sp500'
method = 'RF'
best_model = 'sp500_57.pickle'
############ Parámetros ##################################################
path_datasets = 'path to datasets'
cut = datetime.datetime(1993,1,1)
start_test = datetime.datetime(2014,4,1)
parameters = []
##############################################################################
# Variables usadas para generar las características y el modelo
maxlags = 10
maxdeltas = 10
folds = 10
################################################################################
bestlags = 9
bestdelta = 9
savemodel = False
##############################################################################

if __name__ == "__main__":
    
    # 1 Ejecutamos el algoritmo para obtener los datos financieros de internet, la clase  pystock
    # se encarga de descargar los datos desde internet, una ves descargados la funcion
    # performFeatureSelection() se encarga de obtener las características usando los cuatro criterios de
    # medición de los returns
    
    sys.stdout = open('results.txt', 'w')  
    pystocks.performFeatureSelection(maxdeltas, maxlags, fout, cut, start_test, path_datasets, savemodel, method, folds, parameters)             
    
    ##########################################################################
    # 2- Seleccionamos el mejor parámetro del modelo(AXDELTA, MAXLAGS) del resultado
    print pystocks.checkModel('result.txt')    
    
    ##########################################################################
    # 3- AEjecutamos el entrenamiento con los mejores parámetros del modelo
    
    print pystocks.performSingleShotClassification(
        bestdelta, bestlags, fout, cut, start_test, path_datasets, savemodel, method, parameters)

    ##########################################################################

    end_period = datetime.datetime(2016,7,14) 

    ###### SP500
    symbol = 'S&P-500'
    name = path_datasets + '/sp500.csv'
    prediction = pystocks.getPredictionFromBestModel(
        9, 9, 'sp500', cut, start_test, path_datasets, 'sp500_57.pickle')[0]
    
       









